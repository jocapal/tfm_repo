#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  7 00:52:50 2019

@author: joaquin
"""

import gym
import cv2
import numpy as np
import matplotlib.pyplot as plt
import os
import h5py
from collections import deque
from keras.layers import Dense, Flatten, Conv2D
from keras.models import Sequential
from keras.optimizers import Adam

game = "BreakoutDeterministic-v4"
env = gym.make(game)
env.reset()
steps =  0          
total_reward = 0
i = 0
i_update = i
episodes_sum = 0
exp_batch = []
weights = "/weights.best.hdf5"
mem_h5 = "/data.hdf5"
save_path = "Save/"
experiment = "/Experimento_6/"
filepath = save_path+game+experiment+weights
save_filepath = save_path+game+experiment+mem_h5
med_sup = 0
max_reward = 0
l10 = deque((), maxlen = 10)
Q_values = deque()

if 'Pong' in game:
    med_sup = -21
    max_reward = -21
else:        
    med_sup = 0
    max_reward = 0    
        
if os.path.isfile(save_filepath) is False:
    savedata = h5py.File(save_filepath, 'w')
    savedata.close()        
    print("Restoration point created.")
else:
    print("Restoration point found.")
    
class AI:
    
    four_batch_size = 4
    four_batch_state = deque((), maxlen = four_batch_size)
    s_batch = []
    cnn_shape = (105,80,4)
    batch_shape = (1,105,80,4)
    learning_rate = 0.00025
    action_size = env.action_space.n
    epsilon = 0.05
    gamma = 0.99
    
    def __init__(self):
        self.parallel_model = self.CNN()
                                   
    def CNN(self):
        model = Sequential()
        model.add(Conv2D(32, (8, 8), strides=(4,4), input_shape = AI.cnn_shape, activation='relu'))
        model.add(Conv2D(64, (4, 4), strides=(2,2), activation='relu'))
        model.add(Conv2D(64, (3, 3), strides=(1,1), activation='relu'))
        model.add(Flatten())
        model.add(Dense(512, activation='relu'))
        model.add(Dense(AI.action_size, activation='linear'))
        parallel_model =  model
        parallel_model.compile(loss="mse", optimizer=Adam(lr=AI.learning_rate))    
        parallel_model.summary()
        print("Model CNN construction complete")
        return parallel_model
       
    def load(self):
        model = self.parallel_model
        try:
            model.load_weights(filepath)
            print("\033[1;32;38mModel weights sucessfully loaded")
            print("\033[1;31;38mALL SYSTEMS NOMINAL")
        except:
            print("\033[1;31;38mError: Model values cant be loaded\033[1;37;38m")
                    
    def batcher(four_batch_state):
        AI.s_batch = []
        AI.s_batch = np.asarray(AI.four_batch_state)
        AI.s_batch.shape = AI.batch_shape
         
    def four_batcher(self, exp_batch):           
        AI.four_batch_state.append(exp_batch[2])
        if len(AI.four_batch_state) == AI.four_batch_size:
            AI.batcher(AI.four_batch_state)
                                                              
    def action_selection(self, s_batch):
        random_value = np.random.rand()
        if random_value < AI.epsilon:
            best_action = env.action_space.sample()
            best_action_value = 0
        else:
            action_values = self.parallel_model.predict(s_batch)
            best_action = np.argmax(action_values[0])
            best_action_value = action_values[0, best_action]
        return best_action, best_action_value
                
def image_processing(image):
    processed_image = cv2.resize(image, (105,80))
    processed_image = cv2.cvtColor(processed_image, cv2.COLOR_RGB2GRAY)
    processed_image = processed_image/255.0
    return processed_image

agent = AI() 
episodes = 200                  
agent.load()
     
for i in range (episodes):
    state = env.reset()
    total_reward = 0
    step_counter = 0     
    if i == 0 and steps == 0:
        random_action = env.action_space.sample()
        state1, reward1, done1, lives1 = env.step(random_action)
        done1 = False
        action1 = [random_action, 0.0]
        lives1 = lives1['ale.lives']
        next_state1 = state1
        exp_batch = (image_processing(state1), action1, image_processing(next_state1), reward1, done1, lives1)
        agent.four_batcher(exp_batch)
        agent.four_batcher(exp_batch)
        agent.four_batcher(exp_batch)
        agent.four_batcher(exp_batch)
        reward1 = np.sign(reward1)
        total_reward += reward1
        del state1, action1, next_state1, reward1, done1, exp_batch, random_action    
    for steps in range (10000):
        action = agent.action_selection(AI.s_batch)
        Q_values.append(action[1])
        next_state, reward, done, lives = env.step(action[0])
        lives = lives['ale.lives']  
        total_reward += reward
        exp_batch = (image_processing(state), action, image_processing(next_state), reward, done, lives)        
        agent.four_batcher(exp_batch)
        state = next_state        
        exp_batch = []
        step_counter += 1
        env.render()            
        if done == True:
            i_update += 1
            episodes_sum += total_reward
            if len(l10) < 10:
                l10.append(total_reward)
            if total_reward > max_reward:
                max_reward = total_reward
            else:
                l10.popleft()
                l10.append(total_reward)
            l10_mean = np.mean(l10)
            Q_values_mean = np.mean(Q_values)
            Q_values = []
            med_episodes = round(episodes_sum/((i+1)),2)
            if med_episodes > med_sup:
                med_sup = med_episodes
            print("\033[1;32;38mEpisode: {}/{}, Total steps: {}, Total Score: {}"
                      .format(i, episodes, steps, total_reward))
            print("\033[1;32;38mMax score: {:.2f}, Maximum mean: {:.2f}, Actual mean: {:.2f}, L10 mean: {:.2f}" .format(max_reward, med_sup, med_episodes, l10_mean))

            plt.subplot(3,1,1)
            plt.title('Reward vs episodes')
            plt.plot(i, total_reward, 'ro', label = 'Reward')
            plt.plot(i, med_episodes, 'g.', label = 'Mean')
            plt.plot(i, l10_mean, 'y.', label = 'L10 Mean')
            plt.xlabel('Episodes', fontsize=12)
            plt.ylabel('Reward', fontsize=12)
            if i == 0:
                plt.legend(loc='upper right', bbox_to_anchor=(1.14,0.10), prop={'size':5.5})
            plt.pause(0.05)
            plt.show()
            break          
            
        if steps % 100 == 0:
                    print("\033[1;37;38mEpisode: {}/{}, Steps: {}, Total steps: {}, Score: {}"
                        .format(i, episodes, steps, step_counter, total_reward))

env.close()