#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  6 15:01:36 2019

@author: joaquin
"""

import matplotlib.pyplot as plt
import h5py

game = "BreakoutDeterministic-v4"
save_path = "Save/"
graph_savefile = "/Backup_google_colab/graph_save.hdf5"
graph_filepath = save_path+game+graph_savefile
saved_l10 = []
saved_max_score = 0.0
saved_max_mean = 0
saved_actual_mean = 0
saved_q_mean = 0
saved_reward = 0
saved_episodes = 0
saved_loss = []
i = []
try:
    savefile = h5py.File(graph_filepath, 'r')
    saved_l10 = savefile['l10']
    saved_max_score = savefile['max_score']
    saved_max_mean = savefile['max_mean']
    saved_actual_mean = savefile['med_episodes']
    saved_q_mean = savefile['q_mean']
    saved_reward = savefile['reward']
    saved_episodes = savefile['i']
    saved_loss = savefile['loss_list']
except:
    print("Data cannot be loaded")
    
max_score = saved_max_score[()]   
max_mean = saved_max_mean[()]
Q_values_mean = list(saved_actual_mean[()])
total_reward = list(saved_reward[()])
episodes = saved_episodes[()]-49
loss_plt = list(saved_loss[()])
med_episodes = list(saved_actual_mean[()])
l10_mean = list(saved_l10[()])
savefile.close()
for x in range(episodes):
    i.append(x)
    
plt.ion()
plt.subplot(3,1,1)
plt.plot(i, loss_plt, 'b*')
plt.title('Loss, reward and Q-values vs episodes')
plt.ylabel('Loss', fontsize=12)
plt.subplot(3,1,2)
plt.plot(i, total_reward, 'ro', label = 'Reward')
plt.plot(i, med_episodes, 'g.', label = 'Mean')
plt.plot(i, l10_mean, 'y.', label = 'L10 Mean')
plt.legend(loc='upper right', bbox_to_anchor=(1.14,0.30), prop={'size':5.5})
plt.xlabel('Episodes', fontsize=12)
plt.ylabel('Reward', fontsize=12)
plt.subplot(3,1,3)
plt.plot(i, Q_values_mean, '.m')
plt.xlabel('Episodes', fontsize=12)
plt.ylabel('Q-values', fontsize=12)
plt.show()