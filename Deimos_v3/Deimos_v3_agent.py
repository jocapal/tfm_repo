#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 18 19:55:56 2019

@author: joaquin
"""
###############################################################################
#                               DEIMOS-V3                                     #
#                           Version Single-GPU                                #
#                                                                             #
#                           V3.0 (23/04/2019)                                 #
# Reestructurado el programa con respecto a la v2.15: Ahora se compone de 3   #
# archivos, para hacer el programa mas limpio.                                #
# Eliminado todo lo referente a TensorBoard ya que no se va a utilizar.       #
# Añadida info de memoria cada 10 episodios para tener un control mas preciso #
# sobre que esta pasando.                                                     #
# Arreglado el hecho de que la puntuacion media y maxima en Pong sea siempre  #
# 0.                                                                          #
#                              24/04/2019                                     #
# Movidas funciones a archivo de funciones auxiliares por limpieza.           #
# Ahora el agente solo guardara los ficheros de la CNN si ha terminado el     #
# periodo de observacion.                                                     #
#                              27/04/2019                                     #
# Solucionado problema con el contador de los episodios, que no los contaba   #
# correctamente.                                                              #                                      
###############################################################################

# 1. IMPORTS Y FROMS:
import gym
import numpy as np
import time
import h5py
import random
from Deimos_v3_aux import image_processing, elapsed_time, memory_usage, directory_creation, path_check, load_episodes, load_steps 
from Deimos_v3_metrics import print_progress, print_episode_final, graph_plot
from collections import deque
from keras.layers import Dense, Flatten, Conv2D
from keras.models import Sequential
from keras.optimizers import Adam

# 2. VARIABLES INICIALES:
game = "BreakoutDeterministic-v4"
start_time = time.time()
env = gym.make(game)
env.reset()
steps =  0          
total_reward = 0
i = 0
i_update = i
episodes_sum = 0
exp_batch = []
weights = "/weights.best.hdf5"
mem_h5 = "/data.hdf5"
save_path = "Save/"
filepath = save_path+game+weights
save_filepath = save_path+game+mem_h5
if 'Pong' in game:
    med_sup = -21
    max_reward = -21
else:        
    med_sup = 0
    max_reward = 0
l10 = deque((), maxlen = 10)
Q_values = deque()
episodes = 100010
observation_episodes = 50
update_network = 10000
directory_creation(save_path, game)
path_check(save_filepath)   

# 3. AI: Aqui definiremos el agente como tal; especificaremos cada seccion del
# agente, explicando que realiza cada parte del mismo.
class AI:
# 3.1 Variables iniciales; estas variables estan disponibles para su acceso 
# desde cualquier punto del programa escribiendo AI.nombre_de_la_variable.
# Ejemplo: AI.four_batch_size    
    four_batch_size = 4
    four_memory_state = deque((), maxlen = four_batch_size)
    four_memory_action = deque((), maxlen = four_batch_size)
    four_memory_next_state = deque((), maxlen = four_batch_size)
    four_memory_reward = deque((), maxlen = four_batch_size)
    four_memory_done = deque((), maxlen = four_batch_size)
    four_memory_lives = deque((), maxlen = four_batch_size)
    four_memory_batch = deque((), maxlen = 1)
    four_batch_state = deque((), maxlen = four_batch_size)
    mem_size = 200000 
    mem_batch = deque((), maxlen = mem_size)
    replay_sample_size = 32 
    sample_batch = []
    s_batch = []
    cnn_shape = (105,80,4)
    batch_shape = (1,105,80,4)
    learning_rate = 0.00025
    action_size = env.action_space.n
    initial_epsilon = 1
    final_epsilon = 0.1
    epsilon_frames = 1000000
    epsilon_decay = (initial_epsilon-final_epsilon)/epsilon_frames
    epsilon = initial_epsilon
    loss_list = []
    gamma = 0.99
    loss_plt = 0
    
# 3.2 Variables de inicio: estas variables son comunes dentro de la clase AI, 
# es decir, del agente.
    def __init__(self):
        self.model = self.CNN()
        self.target_model = self.target_CNN()
        
# Aqui intentamos cargar el valor de epsilon en caso de que exista; si no es 
# asi, utilizaremos el valor inicial de epsilon.        
        try:
            savedata = h5py.File(save_filepath, 'r')
            saved_epsilon = savedata['epsilon']
            AI.epsilon = saved_epsilon[()]
            savedata.close()
            print("\033[1;32;38mExploration rate sucessfully loaded")
        except:
            AI.epsilon = AI.epsilon
            savedata.close()
            print("\033[1;31;38mError: exploration rate values cant be loaded")
            
# 3.3 Definimos la red neuronal convolucional:            
    def CNN(self):
        model = Sequential()
        model.add(Conv2D(32, (8, 8), strides=(4,4), input_shape = AI.cnn_shape, activation='relu'))
        model.add(Conv2D(64, (4, 4), strides=(2,2), activation='relu'))
        model.add(Conv2D(64, (3, 3), strides=(1,1), activation='relu'))
        model.add(Flatten())
        model.add(Dense(512, activation='relu'))
        model.add(Dense(AI.action_size, activation='linear'))
        model.compile(loss="mse", optimizer=Adam(lr=AI.learning_rate))
        model.summary()
        print("Model CNN construction complete")
        return model
                   
# 3.4 Definimos la red neuronal objetivo; debe ser identica a la CNN.        
    def target_CNN(self):
        target_model = Sequential()
        target_model.add(Conv2D(32, (8, 8), strides=(4,4), input_shape = AI.cnn_shape, activation='relu'))
        target_model.add(Conv2D(64, (4, 4), strides=(2,2), activation='relu'))
        target_model.add(Conv2D(64, (3, 3), strides=(1,1), activation='relu'))
        target_model.add(Flatten())
        target_model.add(Dense(512, activation='relu'))
        target_model.add(Dense(AI.action_size, activation='linear'))
        target_model.compile(loss="mse", optimizer=Adam(lr=AI.learning_rate))
        target_model.summary()
        print("Target CNN construction complete")
        print("\033[1;32;38mCNN construction completed sucessfully")
        return target_model
                 
# 3.5 Con esta funcion, cargamos los valores de los pesos en la red neuronal;
# de esta forma, podremos retomar el entrenamiento en caso de que paremos el
# programa:        
    def load(self):
        model = self.model
        try:
            model.load_weights(filepath)
            print("\033[1;32;38mModel weights sucessfully loaded")
            print("\033[1;31;38mALL SYSTEMS NOMINAL")
        except:
            print("\033[1;31;38mError: Model values cant be loaded\033[1;37;38m")
            
# 3.6 En esta funcion, guardamos los valores de epsilon, los pesos de las 
# neuronas y el numero de episodios para posteriormente cargarlos en caso de 
# que paremos el programa. Guardamos cada 10 episodios.              
    def save(self, i, steps):
        model = self.model
        exploration_rate = AI.epsilon
        if episodes % 10 == 0:
            model.save(filepath)
            print("\033[1;37;38mModel saved")
            savedata = h5py.File(save_filepath, 'w')
            saved_epsilon = savedata.create_dataset('epsilon', data = AI.epsilon)
            saved_i = savedata.create_dataset('i', data = i_update)
            saved_steps = savedata.create_dataset('steps', data = step_counter)
            savedata.close()
        return exploration_rate, saved_epsilon, saved_i, saved_steps
            
# 3.7 Con esta funcion copiamos los pesos de la red neuronal principal a la red
# neuronal objetivo; este proceso se realiza cada determinado numero de pasos.
    def update_net(self):
        model = self.model
        model_weights = model.get_weights()
        target_model = self.target_model
        target_model.set_weights(model_weights)
        print("Target model sucessfully updated")
        
# 3.8 en la funcion batcher, creamos un paquete especifico con los estados para
# la seleccion de la accion.
    def batcher(four_batch_state):
        AI.s_batch = []
        AI.s_batch = np.asarray(AI.four_batch_state)
        AI.s_batch.shape = AI.batch_shape
        
# 3.9 En esta funcion creamos los paquetes de cuatro experiencias; cuando estan
# hechos los paquetes de 4, los enviamos a las diferentes funciones del
# programa para su posterior procesamiento.        
    def four_batcher(self, exp_batch):           
        AI.four_batch_state.append(exp_batch[2])
        if len(AI.four_batch_state) == AI.four_batch_size:
            AI.batcher(AI.four_batch_state)
            
# 3.10 Esta funcion prepara los distintos paquetes extraidos de las 
# experiencias y los almacena en la memoria para su posterior utilizacion.           
    def four_memory(self, exp_batch):
       AI.four_memory_state.append(exp_batch[0])
       AI.four_memory_action.append(exp_batch[1])
       AI.four_memory_next_state.append(exp_batch[2])
       AI.four_memory_reward.append(exp_batch[3])
       AI.four_memory_done.append(exp_batch[4])
       AI.four_memory_lives.append(exp_batch[5])
       
       if len(AI.four_memory_state) == AI.four_batch_size:
            AI.four_memory_batch.append((AI.four_memory_state, AI.four_memory_action, 
                                    AI.four_memory_next_state, AI.four_memory_reward, 
                                    AI.four_memory_done, AI.four_memory_lives))
            AI.memory(AI.four_memory_batch)
# 3.11 En la funcion memory almacenamos todas las experiencias en paquetes de 4
# listas para su posterior procesamiento. El limite de memoria esta definido 
# por la variable AI.mem_size; esto es importante ya que dependiendo de la ram
# de la que dispongamos, el limite se debera ajustar. Cuando se llena, la 
# funcion automaticamente elimina el elemento mas a la izquierda, es decir, el
# elemento mas antiguo (AI.mem_batch.popleft())                               
    def memory(self):        
        if len(AI.mem_batch) < AI.mem_size:
            AI.mem_batch.append(AI.four_memory_batch)
        else:
            AI.mem_batch.popleft()
            AI.mem_batch.append(AI.four_memory_batch)
            
# 3.12 En la funcion replay_sampler, cogemos las experiencias acumuladas desde
# la memoria, y las procesamos para prepararlas para introducirlas en el
# entrenamiento; hacemos una seleccion aleatoria de entre los elementos de la
# memoria, dependiendo del tamaño de la misma; si es menor que lo especificado
# en AI.replay_sample_size, tomamos una muestra del tamaño de la propia memoria
# (es decir, todas las muestras); si el tamaño de memoria es mayor, tomamos un
# numero de muestras del tamaño establecido en AI.replay_sample_size.            
    def replay_sampler(self, mem_batch):
        AI.sample_batch = []
        
        if len(AI.mem_batch) < AI.replay_sample_size:
            AI.sample_batch.append(random.sample(AI.mem_batch, len(AI.mem_batch)))
        else:
            AI.sample_batch.append(random.sample(AI.mem_batch, AI.replay_sample_size))
            
        AI.sample_batch = np.asarray(AI.sample_batch)
        AI.sample_batch.shape = (AI.replay_sample_size, 6, 4)
                        
# 3.13 En action_selection, como su nombre indica, seleccionamos una accion;
# para ello, usamos un paquete de 4 experiencias procesado anteriormente en la
# funcion batcher; este paquete lo enviamos a la red neuronal principal.
# Despues comparamos el valor de epsilon (nuestro ratio de exploracion) con un
# numero aleatorio. Si el valor aleatorio es menor que el valor de epsilon, 
# tomaremos una accion aleatoria en vez de la calculada con la CNN.                         
    def action_selection(self, s_batch):
        random_value = np.random.rand()
        if random_value < AI.epsilon or i < observation_episodes:
            best_action = env.action_space.sample()
            best_action_value = 0
        else:
            action_values = self.model.predict(s_batch)
            best_action = np.argmax(action_values[0])
            best_action_value = action_values[0, best_action]
# Decrementamos epsilon hasta su valor minimo:            
        if i > observation_episodes:    
            if AI.epsilon > AI.final_epsilon:
                AI.epsilon -= AI.epsilon_decay
        return best_action, best_action_value

# 3.14 En la funcion training, usamos las experiencias acumuladas anteriormente
# para entrenar la red; aqui se implementa el algoritmo Q-Learning propiamente
# dicho. Tambien se calcula el loss, que es la diferencia entre el estado
# actual y el estado predicho en un futuro; esta medida nos da una idea de cuan
# preciso esta siendo nuestro proceso.                
    def training(self, sample_batch):
        AI.training_s_batch = []
        AI.training_a_batch = []
        AI.training_ns_batch = []
        AI.training_r_batch = []
        AI.training_d_batch = []
        Q_values = []
        loss_batch = []
        batch_size = len(AI.sample_batch)
        Q_values = np.zeros((batch_size, AI.action_size))
        new_Q = np.zeros((batch_size, AI.action_size))
        
        for m in range(batch_size):
            AI.training_s_batch = np.asarray(list(AI.sample_batch[m,0])).reshape(AI.batch_shape)
            AI.training_a_batch = np.asarray(list(AI.sample_batch[m,1]))
            AI.training_a_batch = AI.training_a_batch[:,0].astype(int)
            AI.training_ns_batch = np.asarray(list(AI.sample_batch[m,2])).reshape(AI.batch_shape)
            AI.training_r_batch = np.asarray(list(AI.sample_batch[m,3]))
            AI.training_d_batch = np.asarray(list(AI.sample_batch[m,4]))

            loss_batch.append(AI.training_s_batch)
            
            Q_values[m] = self.model.predict(AI.training_s_batch.reshape(AI.batch_shape))
            Q_values[m, AI.training_a_batch] = AI.training_r_batch
# Si ha perdido una vida, se le penaliza; de esta forma se "fuerza" al agente a
# que aprenda a no morir. Para penalizarlo no se le dan mas recompensas si ha 
# perdido una vida. Para Pong, penalizamos si su puntuacion es negativa:
            if 'Pong' in game:
                    new_Q[m] = self.target_model.predict(AI.training_ns_batch.reshape(AI.batch_shape))
                    Q_values[m, AI.training_a_batch] += (AI.gamma * np.max(new_Q))                    
            else: 
                if np.all(AI.training_d_batch) == False:
                    new_Q[m] = self.target_model.predict(AI.training_ns_batch.reshape(AI.batch_shape))
                    Q_values[m, AI.training_a_batch] += (AI.gamma * np.max(new_Q))
                          
        loss = self.model.fit(np.asarray(loss_batch).reshape(batch_size,105,80,4), Q_values, batch_size=batch_size, verbose=0, shuffle=False)
        loss_history = loss.history['loss'][0]
        AI.loss_list.append(loss_history)

        if 'Pong' in game:
            if done == True:
                AI.loss_plt = np.mean(AI.loss_list)
                AI.loss_list = []
        else: 
            if lives == 0:            
                AI.loss_plt = np.mean(AI.loss_list)
                AI.loss_list = []
                
# Antes de entrar en el bucle, cargamos el numero de episodios, epsilon, los 
# pasos, asi como los pesos de las neuronas:                
agent = AI()
episodes = load_episodes(save_filepath, episodes, i_update)[0]
i_update = load_episodes(save_filepath, episodes, i_update)[1]
step_counter = load_steps(save_filepath)
agent.load()
# 4. Bucle de entrenamiento: aqui entrenamos el programa por un numero de
# episodios determinados; dentro de cada episodio, ponemos un limite de pasos
# (acciones) que puede realizar antes de acabar el episodio. El episodio
# tambien puede acabar porque el agente fracasa y no consigue el objetivo.
# Puesto que para que el resto del proceso funcione, si acabamos de arrancar el
# programa, bien sea por primera vez o tras apagarlo, todos los buffers estaran
# vacios, lo cual provocaria un error (no hay ninguna imagen que procesar, ni
# ningun batch que introducir a las CNN). Para solucinarlo, tomamos el primer
# estado (el estado 0) y lo introducimos un numero de veces suficiente como 
# para que el programa funcione adecuadamente. Desde el bucle tambien se 
# monitoriza el avance del mismo, tanto viendo como progresa visualmente
# env.render(), como creando una grafica (reward_plot); tambien mostramos el
# valor de epsilon para darnos una idea de cuanto ha avanzado el programa.
# Otras metricas que se toman, a nivel de debug, es el tamaño de AI.mem_batch,
# ya que si se nos desborda puede bloquear el ordenador, por eso, al menos en
# las primeras sesiones es recomendable controlar el tamaño de memoria.
# Para ver la ram libre, asi como el uso de la CPU (aunque al usar
# procesamiento en paralelo con la GPU es secundario), usamos las utilidades
# proporcionadas por la libreria psutil (psutil.cpu_percent y 
# psutil.virtual_memory). Se muestra por pantalla el episodio en el que esta y
# el numero de pasos que lleva del mismo cada 100 pasos, como control.      
for i in range (episodes):
    state = env.reset()
    total_reward = 0
    if i > observation_episodes:
        if i % 10 == 0:
            agent.save(i_update, AI.epsilon)
            memory_usage()
            print("\033[1;37;38mMemory length: ", len(agent.mem_batch))        
    if i == 0 and steps == 0:
        random_action = env.action_space.sample()
        state1, reward1, done1, lives1 = env.step(random_action)
        done1 = False
        action1 = [random_action, 0.0]
        lives1 = lives1['ale.lives']
        next_state1 = state1
        exp_batch = (image_processing(state1), action1, image_processing(next_state1), reward1, done1, lives1)
        agent.four_batcher(exp_batch)
        agent.four_batcher(exp_batch)
        agent.four_batcher(exp_batch)
        agent.four_batcher(exp_batch)
        reward1 = np.sign(reward1)
        total_reward += reward1
        del state1, action1, next_state1, reward1, done1, exp_batch, random_action
    no_op_counter = 0
    no_op_max = np.random.randint(4,30, dtype=int)
    
    for steps in range (10000):
# NO-OP al inicio, como en el paper de DeepMind:
        if no_op_counter < no_op_max:
            action = [0, 0.0]
            no_op_counter += 1
        else:
            action = agent.action_selection(AI.s_batch)
        Q_values.append(action[1])
        next_state, reward, done, lives = env.step(action[0])
        lives = lives['ale.lives']
        reward = np.sign(reward)
# Si pierde una vida, penalizamos evitando que consiga recompensa maxima; asi 
# lo "enseñamos" a no perder vidas.  
        if (lives < lives1):
            done == True
        total_reward += reward
        exp_batch = (image_processing(state), action, image_processing(next_state), reward, done, lives)        
        agent.four_batcher(exp_batch)
        agent.four_memory(exp_batch)
        state = next_state        
        exp_batch = []
        step_counter += 1
        env.render()
        if i >= observation_episodes:
            agent.replay_sampler(AI.mem_batch)
            agent.training(AI.sample_batch)       
            if step_counter % update_network == 0:
                agent.update_net()               
            if 'Pong' in game:
                if done == True:
                  i_update += 1
                  episodes_sum += total_reward
                  episode_time = time.time()
                  if len(l10) < 10:
                      l10.append(total_reward)
                  if total_reward > max_reward:
                    max_reward = total_reward
                  else:
                    l10.popleft()
                    l10.append(total_reward)
                  l10_mean = np.mean(l10)
                  Q_values_mean = np.mean(Q_values)
                  Q_values = []
                  med_episodes = round((episodes_sum/((i+1)-observation_episodes)),2)
                  h,m,s = elapsed_time(start_time, episode_time)
                  if med_episodes > med_sup:
                      med_sup = med_episodes
                  epsilon = AI.epsilon
                  loss_plt = AI.loss_plt
                  print_episode_final(i, episodes, steps, total_reward, AI.epsilon, AI.loss_plt, h,m,s, max_reward, med_sup, med_episodes, l10_mean, Q_values_mean)
                  graph_plot(i, AI.loss_plt, total_reward, med_episodes, l10_mean, Q_values_mean, observation_episodes)
                  break
# Si es cualquier otro juego:            
            else:
                if lives == 0:
                    i_update += 1
                    episodes_sum += total_reward
                    episode_time = time.time()
                    if len(l10) < 10:
                        l10.append(total_reward)
                    if total_reward > max_reward:
                        max_reward = total_reward
                    else:
                        l10.popleft()
                        l10.append(total_reward)                        
                    l10_mean = np.mean(l10)
                    Q_values_mean = np.mean(Q_values)
                    Q_values = []
                    med_episodes = round((episodes_sum/((i+1)-observation_episodes)),2)
                    h,m,s = elapsed_time(start_time, episode_time)
                    if med_episodes > med_sup:
                        med_sup = med_episodes
                    print_episode_final(i, episodes, steps, total_reward, AI.epsilon, AI.loss_plt, h,m,s, max_reward, med_sup, med_episodes, l10_mean, Q_values_mean)
                    graph_plot(i, AI.loss_plt, total_reward, med_episodes, l10_mean, Q_values_mean, observation_episodes)
        if steps % 100 == 0:
            print_progress(i, episodes, steps, step_counter, total_reward)            
# Adecuamos aqui de nuevo para Pong:
        if 'Pong' in game:
            if done == True:
                break
        else:
            if lives == 0:
                break