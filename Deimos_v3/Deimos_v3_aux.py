#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 18 19:56:11 2019

@author: joaquin
"""
###############################################################################
#                               DEIMOS-V3                                     #
#                           Funciones auxiliares                              #
#                                                                             #
# Este archivo contiene todas las funciones auxiliares que se han podido      #
# mover al mismo sin alterar el correcto funcionamiento del agente.           #
#                              23/04/2019                                     #
# Añadida funcion para obtener tamaño de memoria y uso de la CPU cada 10      #
# episodios.                                                                  #
#                              24/04/2019                                     #
# Añadidas funciones directory_creation, path_check, load_episodes,           #
# load_steps                                                                  #
#                              27/04/2019                                     #
# Ahora la funcion load_episodes devuelve dos valores, el de los episodios y  #
# el de los episodios completados.                                            # 
###############################################################################
import cv2
import psutil
import os
import h5py

# PROCESAMIENTO DE LA IMAGEN
# En image_processing, cogemos cada una de las imagenes que genera el programa
# y las procesamos para dejarlas listas para el resto de tratamiento de datos.
# El procesamiento consiste en lo siguiente: un cambio de tamaño a 105x80 (la 
# imagen original es de tamaño 210x160), pasar la imagen a blanco y negro y a
# escalar los valores de los pixeles a 1 al dividir entre 255.
def image_processing(image):
    processed_image = cv2.resize(image, (105,80))
    processed_image = cv2.cvtColor(processed_image, cv2.COLOR_RGB2GRAY)
    processed_image = processed_image/255.0
    return processed_image

# Esta pequeña funcion nos dara el tiempo de ejecucion conforme vaya avanzando:
def elapsed_time(start_time, episode_time):
    execution_time = episode_time-start_time
    m, s = divmod(execution_time, 60)
    h, m = divmod(m, 60)
    h = int(h)
    m = int(m)
    s = int(s)
    return h, m, s

# Esta funcion nos dira el uso de la CPU, uso de memoria y tamaño del batch de 
# memoria:
def memory_usage():
    print("CPU usage: ", psutil.cpu_percent())
    print(psutil.virtual_memory())

# Esta funcion comprobará si existe la estructura de archivos para el guardado;
# si no existe, la creara.
def directory_creation(save_path, game):
    try: 
        os.makedirs(save_path+game)
        print("Directory", save_path+game, "created.")
    except FileExistsError:
        print("Directory ", save_path+game, "already exists. Variables would be loaded from there.")
        
# Esta funcion comprueba que existe un archivo de guardado. En caso contrario,
# lo crea:        
def path_check(save_filepath):
    if os.path.isfile(save_filepath) is False:
        savedata = h5py.File(save_filepath, 'w')
        savedata.close()        
        print("Restoration point created.")
    else:
        print("Restoration point found.")
        
# Esta funcion carga los episodios guardados antes de empezar el bucle de programa:
def load_episodes(save_filepath, epidoses, i_update):
    episodes = 100010
    try:
        savedata = h5py.File(save_filepath, 'r')
        saved_i = savedata['i']
        i_update = saved_i[()]
        savedata.close()
        episodes = episodes-i_update
        print("\033[1;32;38mEpisodes sucessfully loaded")
    except:
        episodes = episodes
        savedata.close()
        print("\033[1;31;38mEpisodes could not be loaded. Starting at episode 0")
    return episodes, i_update
        
# Esta funcion carga los pasos que hemos dado:        
def load_steps(save_filepath):
    try:
        savedata = h5py.File(save_filepath, 'r')
        saved_steps = savedata['steps']
        step_counter = saved_steps[()]
        savedata.close()
        print("\033[1;32;38mSteps sucessfully loaded")
    except:
        step_counter = 0
        savedata.close()
        print("\033[1;31;38mSteps could not be loaded. Starting at step 0")
    return step_counter