#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  6 21:46:29 2019

@author: joaquin
"""

import gym
import numpy as np
from collections import deque
import matplotlib.pyplot as plt

game = "PongDeterministic-v4"
env = gym.make(game)
env.reset()
episodes = 200
episodes_sum = 0
l10 = deque((), maxlen = 10)
if 'Pong' in game:
    med_sup = -21
    max_reward = -21
else:        
    med_sup = 0
    max_reward = 0
    
for i in range(episodes):
    total_reward = 0
    env.reset()
    for steps in range(10000):        
        action = env.action_space.sample()
        _, reward, done, _ = env.step(action)
        env.render()
        steps += 1
        total_reward += reward
        if done == True:
            episodes_sum += total_reward
            if len(l10) < 10:
                l10.append(total_reward)
            if total_reward > max_reward:
                max_reward = total_reward
            else:
                l10.popleft()
                l10.append(total_reward)
            l10_mean = np.mean(l10)
            med_episodes = round((episodes_sum/(i+1)),2)
            if med_episodes > med_sup:
                med_sup = med_episodes
            print("\033[1;32;38mMax score: {:.2f}, Maximum mean: {:.2f}, Actual mean: {:.2f}, L10 mean: {:.2f}" .format(max_reward, med_sup, med_episodes, l10_mean))
        if steps % 100 == 0:
            print("\033[1;37;38mEpisode: {}/{}, Total steps: {}, Total Score: {}"
                      .format(i, episodes, steps, total_reward))

        if done == True:
            plt.subplot(3,1,1)
            plt.plot(i, total_reward, 'ro', label = 'Reward')
            plt.plot(i, med_episodes, 'g.', label = 'Mean')
            plt.plot(i, l10_mean, 'y.', label = 'L10 Mean')
            plt.xlabel('Episodes', fontsize=12)
            plt.ylabel('Reward', fontsize=12)
            if i == 0:
                plt.legend(loc='upper right', bbox_to_anchor=(1.14,0.10), prop={'size':5.5})
            plt.pause(0.05)
            plt.show()                
            break
env.close()