#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  6 13:12:52 2019

@author: joaquin
"""

import gym
import cv2
import numpy as np
import random
import os
import time
import datetime
import h5py
from collections import deque
from keras.layers import Dense, Flatten, Conv2D
from keras.models import Sequential
from keras.optimizers import Adam

game = "BreakoutDeterministic-v4"
date = str(datetime.datetime.now().strftime('%d-%m-%Y-%H:%M:%S'))
start_time = time.time()
env = gym.make(game)
env.reset()
steps =  0          
total_reward = 0
i = 0
i_update = i
episodes_sum = 0
exp_batch = []
weights = "/weights.best.hdf5"
mem_h5 = "/data.hdf5"
save_path = "Save/"
graph_savefile = "/graph_save.hdf5"
filepath = save_path+game+weights
save_filepath = save_path+game+mem_h5
graph_filepath = save_path+game+graph_savefile
reward_list = []
q_mean_list = []
loss_list = []
l10_list = []
mean_list = []

if 'Pong' in game:
    med_sup = -21
    max_reward = -21
else:        
    med_sup = 0
    max_reward = 0
l10 = deque((), maxlen = 10)
Q_values = deque()
observation_episodes = 50
update_network = 10000

try: 
    os.makedirs(save_path+game)
    print("Directory", save_path+game, "created.")
except FileExistsError:
    print("Directory ", save_path+game, "already exists. Variables would be loaded from there.")
           
if os.path.isfile(save_filepath) is False:
    savedata = h5py.File(save_filepath, 'w')
    savedata.close()        
    print("Restoration point created.")
else:
    print("Restoration point found.")
    
class AI:
   
    four_batch_size = 4
    four_memory_state = deque((), maxlen = four_batch_size)
    four_memory_action = deque((), maxlen = four_batch_size)
    four_memory_next_state = deque((), maxlen = four_batch_size)
    four_memory_reward = deque((), maxlen = four_batch_size)
    four_memory_done = deque((), maxlen = four_batch_size)
    four_memory_lives = deque((), maxlen = four_batch_size)
    four_memory_batch = deque((), maxlen = 1)
    four_batch_state = deque((), maxlen = four_batch_size)
    mem_size = 100000 
    mem_batch = deque((), maxlen = mem_size)
    replay_sample_size = 32 
    sample_batch = []
    s_batch = []
    cnn_shape = (105,80,4)
    batch_shape = (1,105,80,4)
    learning_rate = 0.00025
    action_size = env.action_space.n
    initial_epsilon = 1
    final_epsilon = 0.1
    epsilon_frames = 300000
    epsilon_decay = (initial_epsilon-final_epsilon)/epsilon_frames
    epsilon = initial_epsilon
    loss_list = []
    gamma = 0.99
    loss_plt = 0
    
    def __init__(self):
        self.model = self.CNN()
        self.target_model = self.target_CNN()
               
        try:
            savedata = h5py.File(save_filepath, 'r')
            saved_epsilon = savedata['epsilon']
            AI.epsilon = saved_epsilon[()]
            savedata.close()
            print("\033[1;32;38mExploration rate sucessfully loaded")
        except:
            AI.epsilon = AI.epsilon
            savedata.close()
            print("\033[1;31;38mError: exploration rate values cant be loaded")
                   
    def CNN(self):
        model = Sequential()
        model.add(Conv2D(32, (8, 8), strides=(4,4), input_shape = AI.cnn_shape, activation='relu'))
        model.add(Conv2D(64, (4, 4), strides=(2,2), activation='relu'))
        model.add(Conv2D(64, (3, 3), strides=(1,1), activation='relu'))
        model.add(Flatten())
        model.add(Dense(512, activation='relu'))
        model.add(Dense(AI.action_size, activation='linear'))
        model.compile(loss="mse", optimizer=Adam(lr=AI.learning_rate))
        model.summary()
        print("Model CNN construction complete")
        return model
                           
    def target_CNN(self):
        target_model = Sequential()
        target_model.add(Conv2D(32, (8, 8), strides=(4,4), input_shape = AI.cnn_shape, activation='relu'))
        target_model.add(Conv2D(64, (4, 4), strides=(2,2), activation='relu'))
        target_model.add(Conv2D(64, (3, 3), strides=(1,1), activation='relu'))
        target_model.add(Flatten())
        target_model.add(Dense(512, activation='relu'))
        target_model.add(Dense(AI.action_size, activation='linear'))
        target_model.compile(loss="mse", optimizer=Adam(lr=AI.learning_rate))
        target_model.summary()
        print("Target CNN construction complete")
        print("\033[1;32;38mCNN construction completed sucessfully")
        return target_model
                        
    def load(self):
        model = self.model
        try:
            model.load_weights(filepath)
            print("\033[1;32;38mModel weights sucessfully loaded")
            print("\033[1;31;38mALL SYSTEMS NOMINAL")
        except:
            print("\033[1;31;38mError: Model values cant be loaded\033[1;37;38m")
                         
    def save(self, i, steps):
        model = self.model
        exploration_rate = AI.epsilon
        if episodes % 10 == 0:
            model.save(filepath)
            print("\033[1;37;38mModel saved")
            savedata = h5py.File(save_filepath, 'w')
            saved_epsilon = savedata.create_dataset('epsilon', data = AI.epsilon)
            saved_i = savedata.create_dataset('i', data = i_update)
            saved_steps = savedata.create_dataset('steps', data = step_counter)
            savedata.close()
        return exploration_rate, saved_epsilon, saved_i, saved_steps
            
    def update_net(self):
        model = self.model
        model_weights = model.get_weights()
        target_model = self.target_model
        target_model.set_weights(model_weights)
        print("Target model sucessfully updated")  
        
    def batcher(four_batch_state):
        AI.s_batch = []
        AI.s_batch = np.asarray(AI.four_batch_state)
        AI.s_batch.shape = AI.batch_shape
               
    def four_batcher(self, exp_batch):           
        AI.four_batch_state.append(exp_batch[2])
        if len(AI.four_batch_state) == AI.four_batch_size:
            AI.batcher(AI.four_batch_state)
            
    def four_memory(self, exp_batch):
       AI.four_memory_state.append(exp_batch[0])
       AI.four_memory_action.append(exp_batch[1])
       AI.four_memory_next_state.append(exp_batch[2])
       AI.four_memory_reward.append(exp_batch[3])
       AI.four_memory_done.append(exp_batch[4])
       AI.four_memory_lives.append(exp_batch[5])
       
       if len(AI.four_memory_state) == AI.four_batch_size:
            AI.four_memory_batch.append((AI.four_memory_state, AI.four_memory_action, 
                                    AI.four_memory_next_state, AI.four_memory_reward, 
                                    AI.four_memory_done, AI.four_memory_lives))
            AI.memory(AI.four_memory_batch)
                                          
    def memory(self):        
        if len(AI.mem_batch) < AI.mem_size:
            AI.mem_batch.append(AI.four_memory_batch)
        else:
            AI.mem_batch.popleft()
            AI.mem_batch.append(AI.four_memory_batch)
                                   
    def replay_sampler(self, mem_batch):
        AI.sample_batch = []
        
        if len(AI.mem_batch) < AI.replay_sample_size:
            AI.sample_batch.append(random.sample(AI.mem_batch, len(AI.mem_batch)))
        else:
            AI.sample_batch.append(random.sample(AI.mem_batch, AI.replay_sample_size))
            
        AI.sample_batch = np.asarray(AI.sample_batch)
        AI.sample_batch.shape = (AI.replay_sample_size, 6, 4)          
                                      
    def action_selection(self, s_batch):
        random_value = np.random.rand()
        if random_value < AI.epsilon or i < observation_episodes:
            best_action = env.action_space.sample()
            best_action_value = 0
        else:
            action_values = self.model.predict(s_batch)
            best_action = np.argmax(action_values[0])
            best_action_value = action_values[0, best_action]
# Decrementamos epsilon hasta su valor minimo:            
        if i > observation_episodes:    
            if AI.epsilon > AI.final_epsilon:
                AI.epsilon -= AI.epsilon_decay
        return best_action, best_action_value
               
    def training(self, sample_batch):
        AI.training_s_batch = []
        AI.training_a_batch = []
        AI.training_ns_batch = []
        AI.training_r_batch = []
        AI.training_d_batch = []
        Q_values = []
        loss_batch = []
        batch_size = len(AI.sample_batch)
        Q_values = np.zeros((batch_size, AI.action_size))
        new_Q = np.zeros((batch_size, AI.action_size))
        
        for m in range(batch_size):
            AI.training_s_batch = np.asarray(list(AI.sample_batch[m,0])).reshape(AI.batch_shape)
            AI.training_a_batch = np.asarray(list(AI.sample_batch[m,1]))
            AI.training_a_batch = AI.training_a_batch[:,0].astype(int)
            AI.training_ns_batch = np.asarray(list(AI.sample_batch[m,2])).reshape(AI.batch_shape)
            AI.training_r_batch = np.asarray(list(AI.sample_batch[m,3]))
            AI.training_d_batch = np.asarray(list(AI.sample_batch[m,1]))

            loss_batch.append(AI.training_s_batch)
            
            Q_values[m] = self.model.predict(AI.training_s_batch.reshape(AI.batch_shape))
            Q_values[m, AI.training_a_batch] = AI.training_r_batch

            if 'Pong' in game:
                    new_Q[m] = self.target_model.predict(AI.training_ns_batch.reshape(AI.batch_shape))
                    Q_values[m, AI.training_a_batch] += (AI.gamma * np.max(new_Q))                    
            else: 
                if np.all(AI.training_d_batch) == False:
                    new_Q[m] = self.target_model.predict(AI.training_ns_batch.reshape(AI.batch_shape))
                    Q_values[m, AI.training_a_batch] += (AI.gamma * np.max(new_Q))
                          
        loss = self.model.fit(np.asarray(loss_batch).reshape(batch_size,105,80,4), Q_values, batch_size=batch_size, verbose=0, shuffle=False)
        loss_history = loss.history['loss'][0]
        AI.loss_list.append(loss_history)

        if 'Pong' in game:
            if done == True:
                AI.loss_plt = np.mean(AI.loss_list)
                AI.loss_list = []
        else: 
            if lives == 0:            
                AI.loss_plt = np.mean(AI.loss_list)
                AI.loss_list = []
                
def image_processing(image):
    processed_image = cv2.resize(image, (105,80))
    processed_image = cv2.cvtColor(processed_image, cv2.COLOR_RGB2GRAY)
    processed_image = processed_image/255.0
    return processed_image

def elapsed_time(start_time, episode_time):
    execution_time = episode_time-start_time
    m, s = divmod(execution_time, 60)
    h, m = divmod(m, 60)
    h = int(h)
    m = int(m)
    s = int(s)
    return h, m, s

def save_graph(graph_filepath, l10_mean, max_reward, med_sup, med_episodes, q_mean_list, reward_list, i, loss_list):
    savefile = h5py.File(graph_filepath, 'w')
    saved_l10 = savefile.create_dataset('l10', data = l10_list)
    saved_max_score = savefile.create_dataset('max_score', data = max_reward)
    saved_max_mean = savefile.create_dataset('max_mean', data = med_sup)
    saved_actual_mean = savefile.create_dataset('med_episodes', data = mean_list)
    saved_q_mean = savefile.create_dataset('q_mean', data = q_mean_list)
    saved_reward = savefile.create_dataset('reward', data = reward_list)
    saved_episodes = savefile.create_dataset('i', data = i)
    saved_loss = savefile.create_dataset('loss_list', data = loss_list)
    savefile.close()
    return saved_l10, saved_max_score, saved_max_mean, saved_actual_mean, saved_q_mean, saved_reward, saved_episodes, saved_loss

agent = AI() 
episodes = 100010
try:
        savedata = h5py.File(save_filepath, 'r')
        saved_i = savedata['i']
        i_update = saved_i[()]
        savedata.close()
        episodes = episodes-i_update
        print("\033[1;32;38mEpisodes sucessfully loaded")
except:
        episodes = 100010
        savedata.close()
        print("\033[1;31;38mEpisodes could not be loaded. Starting at episode 0")
try:
    savedata = h5py.File(save_filepath, 'r')
    saved_steps = savedata['steps']
    step_counter = saved_steps[()]
    savedata.close()
    print("\033[1;32;38mSteps sucessfully loaded")
except:
    step_counter = 0
    savedata.close()
    print("\033[1;31;38mSteps could not be loaded. Starting at step 0")             
       
agent.load()
 
for i in range (episodes):
    state = env.reset()
    total_reward = 0
    if i > observation_episodes:
        if i % 10 == 0:
            agent.save(i_update, AI.epsilon)        
    if i == 0 and steps == 0:
        random_action = env.action_space.sample()
        state1, reward1, done1, lives1 = env.step(random_action)
        done1 = False
        action1 = [random_action, 0.0]
        lives1 = lives1['ale.lives']
        next_state1 = state1
        exp_batch = (image_processing(state1), action1, image_processing(next_state1), reward1, done1, lives1)
        agent.four_batcher(exp_batch)
        agent.four_batcher(exp_batch)
        agent.four_batcher(exp_batch)
        agent.four_batcher(exp_batch)
        reward1 = np.sign(reward1)
        total_reward += reward1
        del state1, action1, next_state1, reward1, done1, exp_batch, random_action
    no_op_counter = 0
    no_op_max = np.random.randint(4,30, dtype=int)
    
    for steps in range (10000):
        if no_op_counter < no_op_max:
            action = [0, 0.0]
            no_op_counter += 1
        else:
            action = agent.action_selection(AI.s_batch)
        Q_values.append(action[1])
        next_state, reward, done, lives = env.step(action[0])
        lives = lives['ale.lives']
        reward = np.sign(reward)
        if (lives < lives1):
            done == True
        total_reward += reward
        exp_batch = (image_processing(state), action, image_processing(next_state), reward, done, lives)        
        agent.four_batcher(exp_batch)
        agent.four_memory(exp_batch)
        state = next_state        
        exp_batch = []
        step_counter += 1
        if i >= observation_episodes:
            agent.replay_sampler(AI.mem_batch)
            agent.training(AI.sample_batch)       
            if step_counter % update_network == 0:
                agent.update_net()
            if 'Pong' in game:
                if done == True:
                  i_update += 1
                  episodes_sum += total_reward
                  episode_time = time.time()
                  if len(l10) < 10:
                      l10.append(total_reward)
                  if total_reward > max_reward:
                    max_reward = total_reward
                  else:
                    l10.popleft()
                    l10.append(total_reward)
                  l10_mean = np.mean(l10)
                  Q_values_mean = np.mean(Q_values)
                  q_mean_list.append(Q_values_mean)
                  Q_values = []
                  med_episodes = round((episodes_sum/((i+1)-observation_episodes)),2)
                  h,m,s = elapsed_time(start_time, episode_time)
                  if med_episodes > med_sup:
                      med_sup = med_episodes
                  reward_list.append(total_reward)                         
                  loss_list.append(AI.loss_plt)
                  l10_list.append(l10_mean)
                  mean_list.append(med_episodes)
                  if i > observation_episodes:
                      if i % 10 == 0:
                          save_graph(graph_filepath, l10_mean, max_reward, med_sup, med_episodes, q_mean_list, reward_list, i, loss_list)
                  print("\033[1;32;38mEpisode: {}/{}, Total steps: {}, Total Score: {}, e: {:.4f}, loss: {:.6f}, Execution time: {}:{}:{}"
                      .format(i, episodes, steps, total_reward, agent.epsilon, AI.loss_plt, h,m,s))
                  print("\033[1;32;38mMax score: {:.2f}, Maximum mean: {:.2f}, Actual mean: {:.2f}, L10 mean: {:.2f}, Q-values mean: {:.6f}" .format(max_reward, med_sup, med_episodes, l10_mean, Q_values_mean))         
            else:
                if lives == 0:
                    i_update += 1
                    episodes_sum += total_reward
                    episode_time = time.time()
                    if len(l10) < 10:
                        l10.append(total_reward)
                    if total_reward > max_reward:
                        max_reward = total_reward
                    else:
                        l10.popleft()
                        l10.append(total_reward)                        
                    l10_mean = np.mean(l10)
                    Q_values_mean = np.mean(Q_values)
                    q_mean_list.append(Q_values_mean)
                    Q_values = []
                    med_episodes = round((episodes_sum/((i+1)-observation_episodes)),2)
                    h,m,s = elapsed_time(start_time, episode_time)
                    if med_episodes > med_sup:
                        med_sup = med_episodes                    
                    reward_list.append(total_reward)                         
                    loss_list.append(AI.loss_plt)
                    l10_list.append(l10_mean)
                    mean_list.append(med_episodes)
                    if i > observation_episodes:
                        if i % 10 == 0:
                            save_graph(graph_filepath, l10_mean, max_reward, med_sup, med_episodes, q_mean_list, reward_list, i, loss_list)
                    print("\033[1;32;38mEpisode: {}/{}, Total steps: {}, Total Score: {}, e: {:.4f}, loss: {:.6f}, Execution time: {}:{}:{}"
                      .format(i, episodes, steps, total_reward, agent.epsilon, AI.loss_plt, h,m,s))
                    print("\033[1;32;38mMax score: {:.2f}, Maximum mean: {:.2f}, Actual mean: {:.2f}, L10 mean: {:.2f}, Q-values mean: {:.6f}" .format(max_reward, med_sup, med_episodes, l10_mean, Q_values_mean))
        if steps % 100 == 0:
                    print("\033[1;37;38mEpisode: {}/{}, Steps: {}, Total steps: {}, Score: {}"
                        .format(i, episodes, steps, step_counter, total_reward))
        if 'Pong' in game:
            if done == True:
                break
        else:
            if lives == 0:
                break
