#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 18 19:56:25 2019

@author: joaquin
"""
###############################################################################
#                               DEIMOS-V3                                     #
#                               Metricas                                      #
#                                                                             #
# En este archivo guardaremos las funciones referentes a la obtencion de      #
# datos, graficas, etc.                                                       #
#                               24/04/2019                                    #
# Arreglada leyenda de la grafica.                                            #
###############################################################################
import matplotlib.pyplot as plt

def print_episode_final(i, episodes, steps, total_reward, epsilon, loss_plt, h,m,s, max_reward, med_sup, med_episodes, l10_mean, Q_values_mean):
    print("\033[1;32;38mEpisode: {}/{}, Total steps: {}, Total Score: {}, e: {:.4f}, loss: {:.6f}, Execution time: {}:{}:{}"
                      .format(i, episodes, steps, total_reward, epsilon, loss_plt, h,m,s))
    print("\033[1;32;38mMax score: {:.2f}, Maximum mean: {:.2f}, Actual mean: {:.2f}, L10 mean: {:.2f}, Q-values mean: {:.6f}" .format(max_reward, med_sup, med_episodes, l10_mean, Q_values_mean))

    
def print_progress(i, episodes, steps, step_counter, total_reward):
    print("\033[1;37;38mEpisode: {}/{}, Steps: {}, Total steps: {}, Score: {}"
                        .format(i, episodes, steps, step_counter, total_reward))

# Ploteamos las recompensas y el loss para comprobar como funciona todo:
def graph_plot(i, loss_plt, total_reward, med_episodes, l10_mean, Q_values_mean, observation_episodes):
    plt.ion()
    plt.subplot(3,1,1)
    plt.plot(i, loss_plt, 'b*')
    plt.title('Loss, reward and Q-values vs episodes')
    plt.ylabel('Loss', fontsize=12)
    plt.subplot(3,1,2)
    plt.plot(i, total_reward, 'ro', label = 'Reward')
    plt.plot(i, med_episodes, 'g.', label = 'Mean')
    plt.plot(i, l10_mean, 'y.', label = 'L10 Mean')
    if i == observation_episodes:
        plt.legend(loc='upper right', bbox_to_anchor=(1.14,0.30), prop={'size':5.5})
    plt.xlabel('Episodes', fontsize=12)
    plt.ylabel('Reward', fontsize=12)
    plt.subplot(3,1,3)
    plt.plot(i, Q_values_mean, '.m')
    plt.xlabel('Episodes', fontsize=12)
    plt.ylabel('Q-values', fontsize=12)
    plt.pause(0.05)
    plt.show()